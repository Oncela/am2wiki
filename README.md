This script converts a HTML page of amendments to a wikimedia format for 
the wiki. 

# How to use it 
* Convert your page of amendments to a .html format 
* Open the script in a webbrowser 
* Import amendments 
* Copy paste the result in the wiki

Thank you Arthur for this script !
